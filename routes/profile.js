const express= require('express');
const router = express.Router();
const multer= require('multer');

router.use(express.static('./public'))

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, '/public/upload')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() +'.png')
  }
});

var upload = multer({ storage: storage }).single('image');

router.post('/register', function (req, res) {
  upload(req, res, function (err) {
    if (err) {
      console.log(res);
      console.log(err);
     // return
    }
    res.json({
    	success:true,
    	message: 'image uploaded'
    });
    // Everything went fine
  })
})

module.exports= router;