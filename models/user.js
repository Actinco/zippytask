const mongoose = require('mongoose');

// User Schema
const UserSchema = mongoose.Schema({
  name:{
    type: String,
    required: true

  },
  email:{
    type: String,
    required: true,
    index: { unique: true }
  },
  username:{
    type: String,
    required: true,
    unique:true

  },
  password:{
    type: String,
    required: true,
    min: [6, 'must greater than 5'],
  }, 
  image:{
   data: Buffer, 
   contentType: String 
 }

});

const User = module.exports = mongoose.model('User', UserSchema);
